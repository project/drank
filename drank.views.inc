<?php

/**
 * Sort by Drank rank
 */
class views_handler_sort_drank extends views_handler_sort {
  function query() {
    $this->query->add_orderby('drank');
  }
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['order']['#access'] = FALSE;
  }
}
