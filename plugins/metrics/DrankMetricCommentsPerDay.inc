<?php

$plugin = array(
  'name' => 'Comments Per Day',
  'handler' => array(
    'class' => 'DrankMetricCommentsPerDay',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricCommentsPerDay.php',
  // Drank
  'drank_description' => t("The ratio of comments per day since content creation. Usually penalizes content the older it gets."),
  'drank_requires' => 'comment',
  'drank_vars' => array(
    'score_modifier' => array(
      'name' => 'Score Modifier',
      'default_value' => 8,
    ),
  ),
);
