<?php

$plugin = array(
  'name' => 'Rating',
  'handler' => array(
    'class' => 'DrankMetricRating',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricRating.php',
  // Drank
  'drank_description' => t("The number of votes from the Rate widget. Each vote is 1 score unit."),
  'drank_requires'    => 'rate',
  'drank_vars' => array(
    'score_unit' => array(
      'name' => 'Score Unit',
      'default_value' => 0.125,
    ),
    'widget_id' => array(
      'name' => 'Widget ID',
      'description' => t("Numerical ID of the Rate widget"),
      'default_value' => 1,
    ),
  ),
);
