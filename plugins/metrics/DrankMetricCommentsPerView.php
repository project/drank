<?php

class DrankMetricCommentsPerView extends DrankMetric {

  /**
   * Process the metric
   * @return: score
   */
  public function score($node) {

    // get comment count
    $comment_count = $node->comment_count;

    // get total node views
    $stats = statistics_get($node->nid);
    $total_views = $stats['totalcount'];

    // ratio score
    $score = $this->score_relative_to($comment_count, $total_views);

    // modify score
    $score = $score * $this->vars['score_modifier'];

    // return score
    return $this->validate_score($score);
  }

  /**
   * @param  $node
   * @return boolean
   */
  public function is_data_available($node) {
    if ($node->comment != 0 && isset($node->comment_count)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @return: timestamp the data is valid to
   */
  public function data_end_date($node) {
    // TODO: replace updated with "last interaction"
    return time();
  }
}
