<?php

class DrankMetricRating extends DrankMetric {

  /**
   * Process the metric
   * @return: the score
   */
  public function score($node) {

    // get voting results
    $widget_id = $this->vars['widget_id'];
    $votes     = rate_get_results('node', $node->nid, $widget_id);

    // create score
    $score = $votes['count'] * $this->vars['score_unit'];

    // return score
    return $this->validate_score($score);
  }

  /**
   * Is data there?
   * @return: boolean
   */
  public function is_data_available($node) {
    $widget_id = $this->vars['widget_id'];
    $votes     = rate_get_results('node', $node->nid, $widget_id);

    if (!empty($votes['count'])) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @param $node
   * @return: timestamp
   */
  public function data_end_date($node) {
    return time();
  }
}
