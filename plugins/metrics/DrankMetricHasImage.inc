<?php

$plugin = array(
  'name' => 'Has Image?',
  'handler' => array(
    'class' => 'DrankMetricHasImage',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricHasImage.php',
  // Drank
  'drank_description' => t("Score given if the node has an image."),
  'drank_requires' => 'field',
  'drank_vars' => array(
    'score_unit' => array(
      'name' => 'Score Unit',
      'default_value' => 1.5,
    ),
    'field_name' => array(
      'name' => 'Field Name',
      'description' => t("Something like 'field_[name]'. Include the 'field_'"),
      'default_value' => 'field_image',
    ),
  ),
);
