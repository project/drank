<?php

class DrankMetricCommentsPerDay extends DrankMetric {

  /**
   * Process the metric
   * @return: score
   */
  public function score($node) {

    // get start and end dates
    $created = new DateTime();
    $created->setTimestamp($node->created);
    $updated = new DateTime();
    $updated->setTimestamp($this->data_end_date());

    // calculate difference
    $comment_count  = $node->comment_count;
    $number_of_days = $updated->diff($created)->days;

    // ratio score
    $score = $this->score_relative_to($comment_count, $number_of_days);

    // modify score
    $score = $score * $this->vars['score_modifier'];

    // return score
    return $this->validate_score($score);
  }

  /**
   * @param  $node
   * @return boolean
   */
  public function is_data_available($node) {
    if ($node->comment != 0 && isset($node->comment_count)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @param $node
   * @return: timestamp the data is valid to
   */
  public function data_end_date($node) {
    // TODO: replace updated with "last interaction"
    return time();
  }
}
