<?php

$plugin = array(
  'name' => 'Comments Per View',
  'handler' => array(
    'class' => 'DrankMetricCommentsPerView',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricCommentsPerView.php',
  // Drank
  'drank_description' => t("The ratio of comments per view."),
  'drank_requires' => 'comment',
  'drank_vars' => array(
    'score_modifier' => array(
      'name' => 'Score Modifier',
      'default_value' => 70,
    ),
    'score_limit' => array(
      'name' => 'Score Limit',
      'default_value' => 10,
    ),
  ),
);
