<?php

$plugin = array(
  'name' => 'Views Per Day',
  'handler' => array(
    'class' => 'DrankMetricViewsPerDay',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricViewsPerDay.php',
  // Drank
  'drank_description' => t("The ratio of views per day since content creation."),
  'drank_requires' => 'statistics',
  'drank_vars' => array(
    'score_modifier' => array(
      'name' => 'Score Modifier',
      'default_value' => 0.1,
    ),
  ),
);
