<?php

$plugin = array(
  'name' => 'Comment Count',
  'handler' => array(
    'class' => 'DrankMetricCommentCount',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricCommentCount.php',
  // Drank
  'drank_description' => t("Score unit given per comment."),
  'drank_requires'    => 'comment',
  'drank_vars' => array(
    'score_unit' => array(
      'name' => 'Score Unit',
      'default_value' => 0.02,
    ),
  ),
);
