<?php

class DrankMetricHasImage extends DrankMetric {

  /**
   * Process the metric
   * @return: score
   */
  public function score($node) {

    $field_name = $this->vars['field_name'];
    $image      = $node->${field_name};

    if (!empty($image['und'][0]['fid'])) {
      return $this->vars['score_unit'];
    } else {
      return 0;
    }
  }

  /**
   * Is data there?
   * @return: boolean
   */
  public function is_data_available($node) {

    $field_name = $this->vars['field_name'];

    if (isset($node->${field_name})) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @param $node
   * @return: timestamp
   */
  public function data_end_date($node) {
    return time();
  }
}
