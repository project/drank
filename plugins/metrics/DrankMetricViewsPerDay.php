<?php

class DrankMetricViewsPerDay extends DrankMetric {

  /**
   * Process the metric
   * @return: average views per day as a percentage
   */
  public function score($node) {

    // get total node views
    $stats = statistics_get($node->nid);
    $total_views = $stats['totalcount'];

    // get start and end dates
    $created = new DateTime();
    $created->setTimestamp($node->created);
    $updated = new DateTime();
    $updated->setTimestamp($this->data_end_date($node));

    // calculate difference
    $difference_in_days = $created->diff($updated)->days;

    // save score
    $score = $this->score_relative_to($total_views, $difference_in_days);

    // modify score
    $score = $score * $this->vars['score_modifier'];

    // return score
    return $this->validate_score($score);
  }

  /**
   * @return: boolean - is data there?
   */
  public function is_data_available($node) {
    $stats = statistics_get($node->nid);
    return !empty($stats);
  }

  /**
   * @param $node
   * @return timestamp of last update
   */
  public function data_end_date($node) {
    // TODO: Create 'cut off period' configuration
    $stats = statistics_get($node->nid);
    return $stats['timestamp'];
  }
}
