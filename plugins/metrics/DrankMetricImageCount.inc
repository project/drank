<?php

$plugin = array(
  'name' => 'Image Count',
  'handler' => array(
    'class' => 'DrankMetricImageCount',
    'parent' => 'DrankMetric',
  ),
  'file' => 'DrankMetricImageCount.php',
  // Drank
  'drank_description' => t("Score unit given per image."),
  'drank_requires'    => 'field',
  'drank_vars' => array(
    'score_unit' => array(
      'name' => 'Score Unit',
      'default_value' => 0.5,
    ),
    'field_name' => array(
      'name' => 'Field Name',
      'description' => t("Something like 'field_[name]'. Include the 'field_'"),
      'default_value' => 'field_image',
    ),
  ),
);
