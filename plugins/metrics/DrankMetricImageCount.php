<?php

class DrankMetricImageCount extends DrankMetric {

  /**
   * Process the metric
   * TODO: count images in node's comments
   * @return: score
   */
  public function score($node) {

    // get field from config
    $field_name = $this->vars['field_name'];

    // get images
    if (isset($node->${field_name})) {
      $images = $node->${field_name};

      // create score
      $image_count = count($images['und']);
      $score = $image_count * $this->vars['score_unit'];

      // return score
      return $this->validate_score($score);
    }
    else {
      // return score
      return 0;
    }
  }

  /**
   * Is data there?
   * @return: boolean
   */
  public function is_data_available($node) {

    $field_name = $this->vars['field_name'];

    if (isset($node->${field_name})) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @param  $node
   * @return timestamp
   */
  public function data_end_date($node) {
    return time();
  }
}
