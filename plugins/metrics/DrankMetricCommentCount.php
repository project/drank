<?php

class DrankMetricCommentCount extends DrankMetric {

  /**
   * Process the metric
   * @return: score
   */
  public function score($node) {

    // create score
    $comment_count = $node->comment_count;
    $score = $comment_count * $this->vars['score_unit'];

    // return score
    return $this->validate_score($score);
  }

  /**
   * Is data there?
   * @return boolean
   */
  public function is_data_available($node) {
    if ($node->comment != 0 && isset($node->comment_count)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Date data is valid to
   * @param $node
   * @return timestamp
   */
  public function data_end_date($node) {
    return time();
  }
}
