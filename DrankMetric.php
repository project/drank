<?php

/**
 * Drank Metric
 *
 * Each metric is a ctools plugin.
 * New metrics can be created or existing ones overriden.
 * @see: plugins/metrics
 */
abstract class DrankMetric {

  public $name;
  public $vars;
  public $power;

  function __construct($name, $vars, $power = 1) {
    $this->name  = $name;
    $this->vars  = $vars;
    $this->power = $power;
  }

  /**
   * Process the score.
   *
   * @param  $node: The data source.
   * @return score: The score for this metric.
   */
  abstract public function score($node);

  /**
   * Validate the score.
   *
   * @param  $score: a score
   * @return $score: a score no larger than the score limit
   */
  public function validate_score($score) {
    $score_limit = $this->vars['score_limit'];
    if ($score_limit == 0) {
      return $score;
    }
    else if ($score > $score_limit) {
      return $score_limit;
    }
    else {
      return $score;
    }
  }

  /**
   * Success rate relative to another value.
   *
   * @return: ratio
   */
  public function score_relative_to($subtotal, $total) {
    return ($subtotal / $total);
  }

  /**
   * Modifier.
   *
   * @return: Success rate relative to another value
   */
  public function score_modifier($score) {
    return ($subtotal / $total) * 100;
  }

  /**
   * Is module enabled?
   *
   * @return: boolean
   */
  public function is_module_enabled() {
    return TRUE;
    return module_exists($this->module);
  }

  /**
   * Is metric enabled?
   *
   * @return: boolean
   */
  public function is_metric_enabled() {
    $config_metrics = variable_get("drank_metrics", FALSE);
    if ($config_metrics) {
      if ($config_metrics[$this->name]['enabled']) {
        return TRUE;
      } else {
        return FALSE;
      }
    } else {
      return TRUE;
    }
  }

  /**
   * Is data available?
   *
   * @param: $node
   * @return: boolean
   */
  abstract public function is_data_available($node);

  /**
   * Date data is valid to.
   *
   * @param $node
   * @return timestamp the data is valid to
   */
  abstract public function data_end_date($node);
}
