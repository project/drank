<?php

/**
 * Define metric plugin type
 * Implements hook_ctools_plugin_type()
 */
function drank_ctools_plugin_type() {
  return array(
    'metric' => array(),
  );
}

/**
 * CTools looks for metrics here
 * Implements ctools_plugin_directory()
 */
function drank_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'drank' && $plugin_type == 'metric') {
    return 'plugins/metrics';
  }
}

/**
 * CTools wrapper to load plugins
 */
function drank_get_plugins() {

  ctools_include('plugins');
  $plugins = ctools_get_plugins('drank', 'metric');
  $result  = array();
  foreach ($plugins as $class_name => $info) {
    $result[$class_name] = $info;
  }
  return $result;
}

/**
 * Get enabled metrics
 * @return: enabled metrics
 */
function drank_enabled_plugins() {

  $enabled_plugins = array();
  $all_plugins = drank_get_plugins('drank', 'metric');
  $config = variable_get("drank_metrics", FALSE);

  // Return enabled metrics
  if (isset($config)) {
    foreach ($all_plugins as $class_name => $plugin) {
      // has user enabled metric?
      if ($config[$class_name]['enabled']) {
        // is metric's data source enabled?
        $module = $plugin['drank_requires'];
        if (module_exists($module)) {
          $enabled_plugins[$class_name] = $plugin;
        }
      }
    }
    return $enabled_plugins;
  }
  // Return all metrics if no settings
  else {
    return $all_plugins;
  }
}
