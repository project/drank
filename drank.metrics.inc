<?php

/**
 * Build enabled metrics
 * @return array $metrics : enabled metrics
 */
function drank_build_metrics() {

  $plugins = drank_enabled_plugins();
  $config  = variable_get("drank_metrics", FALSE);
  $metrics = array();

  foreach ($plugins as $class_name => $plugin) {
    if ($class = ctools_plugin_load_class('drank', 'metric', $class_name, 'handler')) {
      // instance name
      $name = $plugin['name'];
      // instance custom variables
      $vars = array();
      foreach ($plugin['drank_vars'] as $var_name => $var) {
        // get custom var from config or class
        $value = isset($config[$class_name][$var_name]) ? $config[$class_name][$var_name] : $var['default_value'];
        $vars[$var_name] = $value;
      }
      // create instance
      $metrics[$class_name] = new $class($name, $vars);
    }
  }
  return $metrics;
}

/**
 * Process metrics
 * @param  array $metrics : DrankMetric instances
 * @return float $rank    : average of scores
 */
function drank_process_metrics($metrics, $node) {
  $total_score = 0;
  $metrics_processed = 0;

  foreach ($metrics as $key => $metric) {
    if ($metric->is_data_available($node)) {
      $total_score += $metric->score($node);
    }
    $metrics_processed++;
  }
  return $total_score / $metrics_processed;
}

/**
 * Update rank for given entity
 *
 * @param string $entity_type : The type of entity such as 'node' or 'user'
 * @param int    $entity_id   : The ID of the entity to update energy for
 * @param string $field_name  : The name of the drank field to update
 */
function drank_save_rank($entity_type, $entity_id, $field_name, $rank) {

  // Get entity
  $entities = entity_load($entity_type, array($entity_id));
  $entity   = reset($entities);

  // Set value
  $entity->{$field_name}[LANGUAGE_NONE][0][DRANK_FIELD_RANK] = $rank;
  $entity->{$field_name}[LANGUAGE_NONE][0][DRANK_FIELD_TIMESTAMP] = time();

  // Ensure that file_field_update() will not trigger additional usage
  unset($entity->revision);

  // Save
  field_attach_presave($entity_type, $entity);
  field_attach_update($entity_type, $entity);

  // Clear entity cache
  entity_get_controller($entity_type)->resetCache(array($entity_id));
}
