# Drank

Drank (Drupal Rank) provides a measure of content quality by collecting metrics
from a range of data sources and then calculating a score which you can sort content by.

**Metrics:**
*italic = implemented*

- *Field: Has Image?*
- *Field: Image Count*
- *Comment: Comment Count*
- *Comment: Comments Per Day*
- *Statistics: Views Per Day*
- Statistics: Views Relative to Website Total
- Statistics: Comments Per View
- Core: Age of node
- Userpoints: The node author's rating
- *Fivestar: rating*
- Facebook: Number of likes
- Flag: Number of positive/negative flags

Metrics are CTools plugins. You can override existing metrics or create your own!

## Different to Radioactivity

Radioactivity measures the content's popularity, whereas Drank measures the content's quality.

Just because you have a lot of eyeballs on something doesn't mean that something is worth viewing. Drank determines the quality of content and puts that in front of users. That will in turn increase its Radioactivity rating, but hopefully avoid a loop of exponentially increasing popularity.

## Setup

1. Enable the module
2. Add a field named "Drank" (machine name: "field_drank") to a content type
3. Leave field settings at their default values and "Number of values" at 1
4. Visit a node and a score will be calculated
5. Create a view with a drank field and sort by "Drank Rank"
