<?php

/**
 * Implements hook_menu().
 */
function drank_menu() {
  $items = array();
  $items['admin/config/search/drank'] = array(
    'title' => 'Drank',
    'description' => 'Configure settings for drank.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drank_admin_form'),
    'access arguments' => array('administer drank'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/search/drank/configure'] = array(
    'title' => 'Configure',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );

  $items['admin/config/search/drank/preview'] = array(
    'title' => 'Preview',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drank_preview_form',
    'access arguments' => array('administer drank'),
  );
  return $items;
}

/**
 * Drank admin form.
 */
function drank_admin_form() {
  $form = array();

  // Load general configuration.
  $drank_config = variable_get("drank_config", FALSE);
  $form['drank_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['drank_config']['drank_cache_expiry'] = array(
    '#title' => 'Cache Expiry Period',
    '#description' => t("Seconds until a node's rank is re-calculated. (300 = 5 mins, 3600 = 1 hour, 86400 = 24 hours)"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $drank_config ? $drank_config['drank_cache_expiry'] : 3600,
  );

  // Load metric configuration.
  $config = variable_get("drank_metrics", FALSE);
  // Build description.
  $description  = "<p>Each metric returns a score.</p>";
  $description .= "<ul>";
  $description .= "<li>Some metrics assign an arbitary value per unit counted. This is called the <em>Score Unit</em>.</li>";
  $description .= "<li>Some metrics are a ratio of 2 values. This ratio can be increased or descreased with a <em>Score Modifier</em>.</li>";
  $description .= "</ul>";
  // Create fieldset for metrics.
  $form['drank_metrics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Metrics'),
    '#description' => t($description),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $plugins = drank_get_plugins('drank', 'metric');
  foreach ($plugins as $class_name => $plugin) {
    // Create fieldset for metric.
    $form['drank_metrics'][$class_name] = array(
      '#type' => 'fieldset',
      '#title' => t($plugin['name']),
      '#description' => $plugin['drank_description'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    // Disabled/enabled status.
    $form['drank_metrics'][$class_name]['enabled'] = array(
      '#title' => t('Enabled?'),
      '#type' => 'checkbox',
      '#default_value' => isset($config[$class_name]['enabled']) ? $config[$class_name]['enabled'] : 0,
    );
    // Disable fieldset if metric not enabled.
    if (empty($config[$class_name]['enabled'])) {
      $form['drank_metrics'][$class_name]['#attributes'] = array(
        'class' => array('metric-disabled'),
      );
    }
    // Disable metric if module not enabled.
    $module = $plugin['drank_requires'];
    if (!module_exists($module)) {
      $form['drank_metrics'][$class_name]['enabled']['#default_value'] = 0;
      $form['drank_metrics'][$class_name]['enabled'] += array(
        '#description' => t("This metric requires the $module module."),
        '#attributes'  => array('disabled' => 'disabled'),
      );
    }
    // Class variables.
    foreach ($plugin['drank_vars'] as $var_name => $var) {
      $form_var =& $form['drank_metrics'][$class_name][$var_name];
      // default value
      if (isset($config[$class_name][$var_name])) {
        $value = $config[$class_name][$var_name];
      } else {
        $value = $var['default_value'];
      }
      // Build field.
      $form_var = array(
        '#title'         => $var['name'],
        '#description'   => $var['description'],
        '#type'          => 'textfield',
        '#default_value' => $value,
      );
      // API variable: score unit.
      if ($var_name == 'score_unit') {
        $form_var['#description'] = t("Each item counted is worth 1 score unit");
      }
      // API variable: modifier.
      if ($var_name == 'score_modifier') {
        $form_var['#description'] = t("Ratio x Modifier = Score. (valid values: 0.1, 1, 50)");
      }
      // API variable: score limit.
      if ($var_name == 'score_limit') {
        $form_var['#description'] = t("Limit the maximum value of this score. Enter 0 for unlimited");
      }
    }
  }
  return system_settings_form($form);
}

/**
 * Drank preview form.
 */
function drank_preview_form() {
  $form = array();

  $drank_preview  = variable_get("drank_preview", FALSE);
  $results_amount = $drank_preview ? $drank_preview['results_amount'] : 25;

  $form['drank_preview'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preview'),
    '#description' => t("<p>Metric scores are averaged to create a rank. Hover over a score to see how much it contributed to the rank.</p>"),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['drank_preview']['results_amount'] = array(
    '#title' => 'Show',
    '#type' => 'select',
    '#options' => array(
      10  => t("10 results"),
      25  => t("25 results"),
      50  => t("50 results"),
      100 => t("100 results"),
    ),
    '#required' => TRUE,
    '#default_value' => $results_amount,
  );
  $form['drank_preview']['tabs'] = array(
    '#type'   => 'horizontal_tabs',
    '#tree'   => TRUE,
    '#prefix' => '<div id="drank-preview">',
    '#suffix' => '</div>',
  );
  // top ranks
  $results_top = drank_results_top($results_amount);
  $form['drank_preview']['tabs']['results_top'] = array(
    '#type' => 'fieldset',
    '#title' => t("Top Ranks"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['drank_preview']['tabs']['results_top']['table'] = array(
    '#markup' => drank_preview_table($results_top),
  );
  // new ranks
  $results_new = drank_results_new($results_amount);
  if ($results_new) {
    $form['drank_preview']['tabs']['results_new'] = array(
      '#type' => 'fieldset',
      '#title' => t("New Ranks"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['drank_preview']['tabs']['results_new']['table'] = array(
      '#markup' => drank_preview_table($results_new),
    );
  }
  return system_settings_form($form);
}

/**
 * Data preview table.
 *
 * @param array $result : entity_id, rank, timestamp (only entity_id used)
 * @param string        : HTML table with ranks and metrics
 */
function drank_preview_table($result) {

  // Total Scores
  $total_score_nodes = 0;
  $scores_by_metric  = array();

  // Build Metrics
  $metrics = drank_build_metrics();

  // Build Table

  // header
  $header = array('Title', 'Rank');
  foreach ($metrics as $class_name => $metric) {
    $header[$class_name] = $metric->name;
  }
  // rows
  $rows = array();
  foreach ($result as $key => $object) {

    $row  = array();
    $node = node_load($object->entity_id);
    $total_score_node  = 0;
    $metrics_processed = 0;

    // node and rank cells
    $row['node'] = l($node->title, "node/$node->nid");
    $row['rank'] = array(
      'header' => 'header',
      'class'  => 'rank',
    );

    // metric cells
    foreach ($metrics as $class_name => $metric) {
      if ($metric->is_data_available($node)) {
        // get score
        $score = $metric->score($node);
        // populate totals
        $total_score_node               += $score;
        $total_score_nodes              += $score;
        $scores_by_metric[$class_name][] = $score;
        // cell
        $cell_class = ($score >= 1)  ? 'high' : 'low';
        $row[$class_name] = array(
          'data'  => drank_format_score($score),
          'class' => array($cell_class, 'score'),
        );
      } else {
        $row[$class_name] = array(
          'data'  => t('no data'),
          'class' => array('no-data', 'score'),
        );
      }
      $metrics_processed++;
    }
    // populate rank column
    $rank = $total_score_node / $metrics_processed;
    $row['rank']['data'] = $rank == 1 ? $rank : number_format($rank, 5);

    // add score's contribution to rank
    foreach ($row as $column_name => $column) {
      if ($column_name == 'node' || $column_name == 'rank') { continue; }
      $score_contrib = drank_preview_table_score_contrib($column['data'], $total_score_node);
      $row[$column_name]['data'] .= $score_contrib;
    }

    // save row
    $rows[] = $row;
  }
  // Sort the multidimensional array
  usort($rows, "drank_sort_by_rank");

  // Average score footer
  $rows[] = drank_preview_table_footer($scores_by_metric, $total_score_nodes);

  // Generate HTML
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Score's contribution to total
 * @param array $score     : the metric's score
 * @param int $total_score : the total of all scores for this metric
 * @return html            : the score's contribution to the total
 */
function drank_preview_table_score_contrib($score, $total_score) {
  $html = '';
  $score_contrib = round(($score / $total_score) * 100);
  $html .= "<span class='score-contrib'>";
  $html .= "$score_contrib%";
  $html .= "</span>";
  return $html;
}

/**
 * Average Score Footer
 * @param array $scores_by_metric  : multidimensional metrics and scores
 * @param float $total_score_nodes : total of all scores from all nodes
 * @return array $row              : row suitable for drupal_theme()
 */
function drank_preview_table_footer($scores_by_metric, $total_score_nodes) {

  // average score conributions
  $row = array();
  $row['heading'] = array(
    'data'    => t("Average Score / Contribution"),
    'header'  => 'header',
    'colspan' => 2,
  );
  foreach ($scores_by_metric as $class_name => $scores) {
    $column_total = array_sum($scores);

    // build row
    $row[$class_name] = array(
      'header' => 'header',
      'class'  => 'footer',
      'data'   => '',
    );
    // average score
    $score_average = drank_format_score($column_total / count($scores));
    $row[$class_name]['data'] .= $score_average;

    // average score contribution
    $score_contrib = drank_preview_table_score_contrib($column_total, $total_score_nodes);
    $row[$class_name]['data'] .= $score_contrib;
  }
  return $row;
}

/**
 * Get top ranks
 * @param  int $results_amount : amount of results to show
 * @return array of objects    : entity_id, rank, timestamp
 */
function drank_results_top($results_amount) {

  // Build query
  $query = db_query(
    "SELECT entity_id, field_drank_rank
    FROM {field_data_field_drank}
    ORDER BY field_drank_rank
    DESC LIMIT {$results_amount}"
  );
  // Get results
  $result = $query->fetchAll();

  // Return results
  return $result;
}

/**
 * Get new ranks
 * @param  int $results_amount : amount of results to show
 * @return array of objects    : entity_id, rank, timestamp
 * @return boolean             : if no results
 */
function drank_results_new($results_amount) {

  // Build query
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->fieldCondition('field_drank', 'rank', 'NULL', '!=')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, $results_amount)
    ->addMetaData('account', user_load(1));

  // Get results
  $results = $query->execute();
  if (isset($results['node'])) {

    // get nids
    $node_ids = array_keys($results['node']);

    // build array of objects
    $objects = array();
    foreach ($node_ids as $key => $nid) {
      $objects[] = (object) array('entity_id' => $nid);
    }
    // return objects
    return $objects;
  } else {
    return FALSE;
  }
}

/**
 * Format score
 * @param  float $score
 * @return number to 2 decimal places or whole numer if integer
 */
function drank_format_score($score) {
  if ($score == intval($score)) {
    return floatval($score);
  } else {
    return number_format(round($score, 2), 2);
  }
}

/**
 * Sort arrays descending by a key
 * @param $a: an array with a 'rank' key
 * @param $b: an array with a 'rank' key
 */
function drank_sort_by_rank($a, $b) {
  return $a['rank'] < $b['rank'];
}
