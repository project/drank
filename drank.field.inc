<?php
// Documentation:
// https://clikfocus.com/blog/how-to-set-up-custom-field-type-using-drupal-7-fields-api
// https://evolvingweb.ca/blog/poutine-maker-an-introduction-to-field-api-drupal-7-part-1

define("DRANK_FIELD_TYPE", "drank");
define("DRANK_WIDGET"    , "drank_widget");
define("DRANK_FORMATTER" , "drank_formatter");

/**
 * Implements hook_field_info()
 */
function drank_field_info() {
  return array(
    DRANK_FIELD_TYPE => array(
      'label' => t('Drank'),
      'description' => t('This field stores a rank in the database.'),
      'settings' => array(),
      'instance_settings' => array(
        'profile' => 'default',
        'history' => 0,
        'history_limit' => 8,
      ),
      'default_widget' => DRANK_WIDGET,
      'default_formatter' => 'text_default',
      'property_type' => 'decimal',
      'microdata' => TRUE,
    ),
  );
}

// FIELD INPUT

/**
 * Implements hook_field_widget_info()
 */
function drank_field_widget_info() {
  return array(
    DRANK_WIDGET => array(
      'label' => t('Default'),
      'field types' => array(DRANK_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_widget()
 */
function drank_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta = 0, $element) {

  if (!user_access("edit drank")) {
    return $element;
  }

  $item =& $items[0];
  $rank = isset($item[DRANK_FIELD_RANK]) ? $item[DRANK_FIELD_RANK] : 0;

  $element += array(
    '#type' => 'fieldset',
    '#group' => 'Drank',
    '#title' => 'Drank',
    DRANK_FIELD_RANK => array(
      '#type' => 'textfield',
      '#title' => t("Rank"),
      '#default_value' => $rank,
      '#attributes' => array('readonly' => 'readonly'),
    ),
    DRANK_FIELD_TIMESTAMP => array(
      '#type' => 'hidden',
      '#title' => t("When the Drank Rank was calculated"),
      '#default_value' => time(),
    ),
  );
  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function drank_field_is_empty($item, $field) {
  return strlen($item[DRANK_FIELD_RANK]) == 0;
}
